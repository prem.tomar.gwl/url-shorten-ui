# Create image based on the official Node 10 image from dockerhub
FROM node:10 AS builder
RUN mkdir -p /app
COPY . ./app
WORKDIR /app
RUN npm i
RUN $(npm bin)/ng build

FROM nginx:1.15.8-alpine
COPY --from=builder /app/dist/angular-starter/ /usr/share/nginx/htm

# # Create a directory where our app will be placed


# # Change directory so that our commands run inside this new directory
# WORKDIR /app

# # Copy dependency definitions
# COPY package*.json /app/

# # Install dependecies
# RUN npm install

# # Get all the code needed to run the app
# COPY . /app/

# # Expose the port the app runs in
EXPOSE 4200

# # Serve the app
# CMD ["npm", "start"]
