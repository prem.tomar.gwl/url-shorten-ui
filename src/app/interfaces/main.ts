export class MainRequest {
  url: string;
  customUrl?: string;
}

export interface MainResponse {
  url: string;
}
