import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MainService } from './services/main.service';
import { MainRequest, MainResponse } from './interfaces/main';
import { error } from 'protractor';
import { Formatter } from './formatter'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'URL Shortner';
  url: string = "https://google.co.in"
  urlToShort: MainRequest;
  form: FormGroup;
  constructor(private builder: FormBuilder,
    private service: MainService,
    private formatter: Formatter) {
    this.urlToShort = new MainRequest()
    this.form = this.builder.group({
      original_url: new FormControl('', [Validators.required, Validators.pattern("(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?")]),
      custom_url: new FormControl('')
    })
  }

  makeItShort = () => {
    this.formatter.formatRequest(this.urlToShort)
      .then(this.service.getShortUrl)
      .then((data: MainResponse) => {
        this.url = data.url
      })
      .catch(error => {
        throw error;
      })
  }

}
