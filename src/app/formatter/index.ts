import { MainRequest } from '../interfaces/main';

export class Formatter {
  constructor() { }

  formatRequest = (data: MainRequest): Promise<MainRequest> => {
    return new Promise((resolve, reject) => {
      if (data)
        resolve(data as MainRequest)
      else
        reject(new Error("Data not provided"))
    })
  }
}
