import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { MainRequest, MainResponse } from '../interfaces/main';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MainService {

  constructor(private http: HttpClient) { }

  /**
   * @param body Pass request data object to send along with post request
   * @description To get short url of original url 
   */
  getShortUrl = (body: MainRequest): Promise<MainResponse> => {

    console.log({ body })
    return new Promise((resolve, reject) => {
      this.http.post<MainResponse>(environment.apis.getShortUrl, body, {
        headers: { "Content-type": "application/json" }
      }).subscribe(data => {
        resolve(data as MainResponse)
      }, error => {
        reject(error);
      })
    })
  }
}
